golang-github-masterzen-winrm (0.0~git20200615.c42b513-1) unstable; urgency=medium

  [ Alexandre Viau ]
  * Point Vcs-* urls to salsa.debian.org.

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Repository, Repository-Browse.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.

  [ Shengjing Zhu ]
  * Update uscan watch file
  * Update maintainer and uploader email address
  * New upstream version 0.0~git20200615.c42b513
  * Update Depends for new version
  * Stop converting markdown doc
  * Bump debhelper-compat to 13
  * Bump Standards-Version to 4.5.0
  * Add Rules-Requires-Root
  * Remove patches no longer needed

 -- Shengjing Zhu <zhsj@debian.org>  Sun, 28 Jun 2020 01:01:33 +0800

golang-github-masterzen-winrm (0.0~git20170601.0.1ca0ba6-3) unstable; urgency=medium

  * Team upload.
  * add 0002-go1.10-ftbfs.patch (Closes: #890934)

 -- Michael Stapelberg <stapelberg@debian.org>  Sun, 25 Feb 2018 15:55:35 +0100

golang-github-masterzen-winrm (0.0~git20170601.0.1ca0ba6-2) unstable; urgency=medium

  * Add 0001-Use-golang-stdlib-instead-of-azure-fork.patch,
    so we can get rid of depending on old golang-github-azure-azure-sdk-for-go
  * Drop 0001-Change-azure-sdk-for-go-to-azure-version.patch
  * Bump Standards-Version to 4.0.1
    + Change priority to optional

 -- Shengjing Zhu <i@zhsj.me>  Wed, 16 Aug 2017 16:34:22 +0800

golang-github-masterzen-winrm (0.0~git20170601.0.1ca0ba6-1) unstable; urgency=medium

  * Adopt package (Closes: #865331).
  * New snapshot release.
  * d/compat: bump to v10.
  * d/control:
    + Bump Standards-Version to 4.0.0.
    + Bump dependency of debhelper to 10.
    + Add autopkgtest-pkg-go Testsuite.
  * d/copyright:
    + Change format url to https.
    + Add myself to copyright holder.
  * Binary package winrm is split from this project by upstream.

 -- Shengjing Zhu <i@zhsj.me>  Tue, 25 Jul 2017 10:31:05 +0800

golang-github-masterzen-winrm (0.0~git20160323.0752679-3) unstable; urgency=medium

  * deb/rules: use override_dh_install-arch to prevent FTBFS in
    arch-indep builds (Closes: #853261).

 -- Daniel Stender <stender@debian.org>  Tue, 31 Jan 2017 01:45:02 +0100

golang-github-masterzen-winrm (0.0~git20160323.0752679-2) unstable; urgency=medium

  * solve binaries-have-conflict with txwinrm (Closes: #848234):
    + rename /usr/bin/winrm via override for dh_install in deb/rules.
    + add debian/winrm.NEWS with info on that change.
    + updated manpage.
  * deb/control:
    + corrected description text.
    + bumped standards version to 3.9.8 (no changes needed).
  * add deb/watch (uses Eriberto's pseudopackage for "No-Release").

 -- Daniel Stender <stender@debian.org>  Thu, 19 Jan 2017 18:59:25 +0100

golang-github-masterzen-winrm (0.0~git20160323.0752679-1) unstable; urgency=medium

  * Initial release (Closes: #808773).

 -- Daniel Stender <stender@debian.org>  Wed, 13 Apr 2016 15:06:16 +0200
